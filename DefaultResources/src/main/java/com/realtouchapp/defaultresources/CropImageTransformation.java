package com.realtouchapp.defaultresources;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.RectF;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

/**
 * Created by C.C.Chang on 2017/6/5.
 *
 * @version 1
 */

public class CropImageTransformation extends BitmapTransformation {
    private final static String TAG = CropImageTransformation.class.getSimpleName();

    private RectF clipRegion;
    private int index;

    private int[] originImageSize;

    public CropImageTransformation(Context context, RectF clipRegion, int index, int[] originImageSize) {
        super(context);

        this.clipRegion = clipRegion;
        this.index = index;
        this.originImageSize = originImageSize;

    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        if (null == clipRegion) {
            return toTransform;
        }

        int originWidth = originImageSize[0];
        int originHeight = originImageSize[1];
        float scale = Math.max(toTransform.getWidth()/((float) originWidth), toTransform.getHeight()/((float) originHeight));

        clipRegion.top *= scale;
        clipRegion.left *= scale;
        clipRegion.right *= scale;
        clipRegion.bottom *= scale;

        Bitmap bm = Bitmap.createBitmap(toTransform,
                                        (int) clipRegion.left,
                                        (int) clipRegion.top,
                                        (int) clipRegion.width(),
                                        (int) clipRegion.height());

        /*
        float scale = Math.max(outWidth/((float) toTransform.getWidth()), outHeight/((float) toTransform.getHeight()));
        Bitmap scaled = Bitmap.createScaledBitmap(toTransform,
                                                    (int) (toTransform.getWidth()*scale),
                                                    (int) (toTransform.getHeight()*scale),
                                                    true);
        if (null == clipRegion) {
            return scaled;
        }

        int originWidth = originImageSize[0];
        int originHeight = originImageSize[1];
        scale *= Math.max(toTransform.getWidth()/((float) originWidth), toTransform.getHeight()/((float) originHeight));

        clipRegion.top *= scale;
        clipRegion.left *= scale;
        clipRegion.right *= scale;
        clipRegion.bottom *= scale;

        Bitmap bm = Bitmap.createBitmap(scaled,
                                        (int) clipRegion.left,
                                        (int) clipRegion.top,
                                        (int) clipRegion.width(),
                                        (int) clipRegion.height());
        */

        return bm;
    }

    @Override
    public String getId() {
        return "com.realtouchapp.crop."+index;
    }

}
