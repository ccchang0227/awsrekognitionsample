package com.realtouchapp.defaultresources;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by C.C.Chang on 2017/6/5.
 *
 * @version 1
 */

public class SubHeadView extends FrameLayout {

    public ImageView iv_content;

    public SubHeadView(@NonNull Context context) {
        super(context);

        iv_content = new ImageView(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.setMargins(5, 5, 5, 5);
        addView(iv_content, params);

    }

    public SubHeadView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        iv_content = new ImageView(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.setMargins(5, 5, 5, 5);
        addView(iv_content, params);

    }

    public SubHeadView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        iv_content = new ImageView(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.setMargins(5, 5, 5, 5);
        addView(iv_content, params);

    }

}
