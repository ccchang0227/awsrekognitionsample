package com.realtouchapp.defaultresources;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by C.C.Chang on 2017/6/9.
 *
 * @version 1
 */

public class UtilsBase {

    public static String getPath(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            String[] projection = { MediaStore.MediaColumns.DATA };
            cursor = context.getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String path = cursor.getString(column_index);
                cursor.close();

                return path;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (null != cursor) {
            cursor.close();
        }

        return null;
    }

    public static byte[] getBytes(Context context, Uri uri) throws IOException {
        InputStream inputStream = context.getContentResolver().openInputStream(uri);
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len;
        if (inputStream != null) {
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            inputStream.close();
        }

        return byteBuffer.toByteArray();
    }

    public static int[] getBitmapSize(String path) {
        if (null == path) {
            return new int[]{0, 0};
        }

        int[] size = new int[]{0, 0};

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        size[0] = options.outWidth;
        size[1] = options.outHeight;

        return size;
    }

}
