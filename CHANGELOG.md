# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.2] - 2017-06-09
### Added
- 新增App Module: 使用Web API進行人臉辨識

### Changed
- 修改`Glide`的Cache方式 (選圖片前先清空Cache)
- 調整專案架構: 共用的部分獨立android library module管理

## [1.1] - 2017-06-06
### Fixed
- 修正無法解析拍照取得的照片大小的bug

### Changed
- 調整部分字樣為中文

## [1.0] - 2017-06-05
### Added
- 使用AWS-Rekognition服務來進行人臉的年齡、性別辨識