package com.realtouchapp.awsrekognitionsample;

import android.content.Context;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognitionClient;
import com.realtouchapp.defaultresources.UtilsBase;

/**
 * Created by C.C.Chang on 2017/6/3.
 *
 * @version 1
 */

public class Utils extends UtilsBase {

    private static AmazonRekognitionClient rekognitionClient;
    private static CognitoCachingCredentialsProvider sCredProvider;

    /**
     * Gets an instance of CognitoCachingCredentialsProvider which is
     * constructed using the given Context.
     *
     * @param context An Context instance.
     * @return A default credential provider.
     */
    private static synchronized CognitoCachingCredentialsProvider getCredProvider(Context context) {
        if (sCredProvider == null) {
            sCredProvider = new CognitoCachingCredentialsProvider(
                    context.getApplicationContext(),
                    Constants.COGNITO_POOL_ID,
                    Regions.fromName(Constants.COGNITO_POOL_REGION));
        }

        return sCredProvider;
    }

    public static synchronized AmazonRekognitionClient getRekognitionClient(Context context) {
        if (rekognitionClient == null) {
            rekognitionClient = new AmazonRekognitionClient(getCredProvider(context.getApplicationContext()));
            rekognitionClient.setRegion(Region.getRegion(Regions.fromName(Constants.COGNITO_POOL_REGION)));
        }

        return rekognitionClient;
    }

}
