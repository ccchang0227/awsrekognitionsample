package com.realtouchapp.awsrekognitionsample;

/**
 * Created by C.C.Chang on 2017/6/3.
 *
 * @version 1
 */

public class Constants {

    /*
     * You should replace these values with your own. See the README for details
     * on what to fill in.
     */
    public static final String COGNITO_POOL_ID = "us-east-1:ef92978a-aeac-4444-a9a6-0c47182bc3fe";

    /*
     * Region of your Cognito identity pool ID.
     */
    public static final String COGNITO_POOL_REGION = "us-east-1";


}
