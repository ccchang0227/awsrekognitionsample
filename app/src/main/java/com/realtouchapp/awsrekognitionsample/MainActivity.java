package com.realtouchapp.awsrekognitionsample;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amazonaws.services.rekognition.AmazonRekognitionClient;
import com.amazonaws.services.rekognition.model.BoundingBox;
import com.amazonaws.services.rekognition.model.DetectFacesRequest;
import com.amazonaws.services.rekognition.model.DetectFacesResult;
import com.amazonaws.services.rekognition.model.Emotion;
import com.amazonaws.services.rekognition.model.FaceDetail;
import com.amazonaws.services.rekognition.model.Image;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.realtouchapp.defaultresources.CropImageTransformation;
import com.realtouchapp.defaultresources.LoadingDialog;
import com.realtouchapp.defaultresources.SubHeadView;
import com.realtouchapp.defaultresources.UserInterfaceTool;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {
    private final static String TAG = MainActivity.class.getSimpleName();

    private final static int REQUEST_CAMERA = 5566;
    private final static int SELECT_FILE = 7788;

    private static class ResourceImage {
        public String title;
        public int resID;

        public ResourceImage(String title, int resID) {
            super();

            this.title = title;
            this.resID = resID;
        }

        public int[] getBitmapSize(Context context) {
            int[] size = new int[]{0, 0};
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), resID, options);

                size[0] = options.outWidth;
                size[1] = options.outHeight;

            } catch (Throwable e) {
                e.printStackTrace();
            }

            return size;
        }

        public byte[] getBytes(Context context) {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                Bitmap bm = BitmapFactory.decodeResource(context.getResources(), resID, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                return stream.toByteArray();
            } catch (Throwable e) {
                e.printStackTrace();
            }

            return null;
        }

    }
    private final static ResourceImage resourceImages[] = {
            new ResourceImage("兩個女人", R.mipmap.binfun),
            new ResourceImage("三個人(暗)", R.mipmap.dark_img),
            new ResourceImage("狗狗", R.mipmap.dog),
            new ResourceImage("吉芳", R.mipmap.fang),
            new ResourceImage("可愛的幼女", R.mipmap.loli),
            new ResourceImage("錢錢", R.mipmap.no_face),
            new ResourceImage("柏翰", R.mipmap.pohan),
            new ResourceImage("花形", R.mipmap.shinting),
            new ResourceImage("女人1", R.mipmap.woman1),
            new ResourceImage("女人2", R.mipmap.woman2),
            new ResourceImage("三課", R.mipmap.group),
            new ResourceImage("健中", R.mipmap.group2),
            new ResourceImage("很多人", R.mipmap.many_people)
    };

    private View btn_upload;
    private ImageView iv_origin;
    private TextView tv_origin_image_size;

    private LinearLayout ll_small_faces;
    private TextView tv_face_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iv_origin = (ImageView) findViewById(R.id.iv_origin);
        tv_origin_image_size = (TextView) findViewById(R.id.tv_origin_image_size);

        ll_small_faces = (LinearLayout) findViewById(R.id.ll_small_faces);
        tv_face_info = (TextView) findViewById(R.id.tv_face_info);

        btn_upload = findViewById(R.id.btn_upload);
        registerForContextMenu(btn_upload);
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final CharSequence[] items = { "Take Photo",
                                                "Choose from Library",
                                                "From Resource",
                                                "Cancel" };

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Add Photo!");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @SuppressWarnings("ResultOfMethodCallIgnored")
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        switch (item) {
                            case 0: {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                                try {
                                    if (f.exists()) {
                                        f.delete();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                                startActivityForResult(intent, REQUEST_CAMERA);

                                break;
                            }
                            case 1: {
                                Intent intent = new Intent(
                                        Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                intent.setType("image/*");
                                startActivityForResult(
                                        Intent.createChooser(intent, "Select File"),
                                        SELECT_FILE);

                                break;
                            }
                            case 2: {
                                openContextMenu(v);

                                break;
                            }
                            default: {
                                dialog.dismiss();

                                break;
                            }
                        }

                    }
                });
                builder.show();
            }
        });

    }

    @Override
    protected void onDestroy() {
        unregisterForContextMenu(btn_upload);

        super.onDestroy();
    }

    private MenuItem menu_Cancel;
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        for (int i = 0; i < resourceImages.length; i ++) {
            menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, resourceImages[i].title);
        }
        menu_Cancel = menu.add(Menu.NONE, Menu.FIRST+resourceImages.length, Menu.NONE, "取消");

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (!item.equals(menu_Cancel)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Glide.get(MainActivity.this).clearDiskCache();
                }
            }).start();
            Glide.get(MainActivity.this).clearMemory();

            int index = item.getItemId()-Menu.FIRST;
            ResourceImage resourceImage = resourceImages[index];
            detectResult.uri = null;
            detectResult.resourceImage = resourceImage;

            Glide.with(MainActivity.this)
                    .load(resourceImage.resID)
                    .fitCenter()
                    .into(iv_origin);

            int[] size = resourceImage.getBitmapSize(MainActivity.this);
            tv_origin_image_size.setText("解析度: " + size[0] + "x" + size[1]);
            detectResult.imageSize = size;

            detectFacesTask = new DetectFacesTask(MainActivity.this, resourceImage);
            detectFacesTask.execute();

        }
        menu_Cancel = null;

        return super.onContextItemSelected(item);
    }

    private File getTempPictureFile() {
        File f = new File(Environment.getExternalStorageDirectory().toString());
        for (File temp : f.listFiles()) {
            if (temp.getName().equals("temp.jpg")) {
                return temp;
            }
        }

        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Glide.get(MainActivity.this).clearDiskCache();
                }
            }).start();
            Glide.get(MainActivity.this).clearMemory();

            if (requestCode == REQUEST_CAMERA) {
                File f = getTempPictureFile();
                if (null == f) {
                    return;
                }

                Uri selectedImageUri = Uri.fromFile(f);
                detectResult.uri = selectedImageUri;
                detectResult.resourceImage = null;

                Glide.with(MainActivity.this)
                        .load(selectedImageUri)
                        .fitCenter()
                        .into(iv_origin);

                int[] size = Utils.getBitmapSize(f.getAbsolutePath());
                tv_origin_image_size.setText("解析度: " + size[0] + "x" + size[1]);
                detectResult.imageSize = size;

                detectFacesTask = new DetectFacesTask(MainActivity.this, selectedImageUri);
                detectFacesTask.execute();

            }
            else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                detectResult.uri = selectedImageUri;
                detectResult.resourceImage = null;

                Glide.with(MainActivity.this)
                        .load(selectedImageUri)
                        .fitCenter()
                        .into(iv_origin);

                int[] size = Utils.getBitmapSize(Utils.getPath(MainActivity.this, selectedImageUri));
                tv_origin_image_size.setText("解析度: " + size[0] + "x" + size[1]);
                detectResult.imageSize = size;

                detectFacesTask = new DetectFacesTask(MainActivity.this, selectedImageUri);
                detectFacesTask.execute();

            }

        }

    }

    private void configureDetectResult() {
        if (null == detectResult.detectFacesResult) {
            return;
        }

        List<FaceDetail> faceDetails = detectResult.detectFacesResult.getFaceDetails();

        if (null != faceDetails && faceDetails.size() > 0) {

            int originWidth = detectResult.imageSize[0];
            int originHeight = detectResult.imageSize[1];

            int clipImageSize = UserInterfaceTool.getPixelFromDpByDevice(MainActivity.this, 100);

            for (int i = 0; i < faceDetails.size(); i ++) {
                FaceDetail detail = faceDetails.get(i);

                SubHeadView shv_subImg = new SubHeadView(MainActivity.this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(clipImageSize-20, clipImageSize-20);
                params.setMargins(10, 10, 10, 10);
                ll_small_faces.addView(shv_subImg, params);

                RectF clipRegion = getClipRegion(detail, originWidth, originHeight);
                BitmapTransformation transformation = new CropImageTransformation(MainActivity.this, clipRegion, i, detectResult.imageSize);

                if (null != detectResult.uri) {
                    Glide.with(MainActivity.this)
                            .load(detectResult.uri)
                            .transform(transformation)
                            .into(shv_subImg.iv_content);
                }
                else if (null != detectResult.resourceImage) {
                    Glide.with(MainActivity.this)
                            .load(detectResult.resourceImage.resID)
                            .transform(transformation)
                            .into(shv_subImg.iv_content);
                }

                shv_subImg.setTag(i+1);
                shv_subImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int i = 0; i < ll_small_faces.getChildCount(); i ++) {
                            View child = ll_small_faces.getChildAt(i);
                            if (child == v) {
                                child.setBackgroundColor(Color.RED);
                            }
                            else {
                                child.setBackgroundColor(Color.TRANSPARENT);
                            }
                        }

                        int index = (int) v.getTag()-1;
                        List<FaceDetail> faceDetails = detectResult.detectFacesResult.getFaceDetails();
                        FaceDetail detail = faceDetails.get(index);
                        configureResultText(detail);
                    }
                });

                if (i == 0) {
                    shv_subImg.setBackgroundColor(Color.RED);
                    configureResultText(detail);
                }
                else {
                    shv_subImg.setBackgroundColor(Color.TRANSPARENT);
                }

            }

        }
        else {
            configureResultText(null);
        }

    }

    private RectF getClipRegion(FaceDetail detail, int originWidth, int originHeight) {
        if (null == detail) {
            return new RectF(0, 0, originWidth, originHeight);
        }
        BoundingBox boundingBox = detail.getBoundingBox();
        if (null == boundingBox) {
            return new RectF(0, 0, originWidth, originHeight);
        }

        float left = boundingBox.getLeft()*originWidth;
        if (left < 0) {
            left = 0;
        }
        float top = boundingBox.getTop()*originHeight;
        if (top < 0) {
            top = 0;
        }
        float right = left + boundingBox.getWidth()*originWidth;
        if (right > originWidth) {
            right = originWidth;
        }
        float bottom = top + boundingBox.getHeight()*originHeight;
        if (bottom > originHeight) {
            bottom = originHeight;
        }

        return new RectF(left, top, right, bottom);
    }

    @SuppressLint("DefaultLocale")
    private void configureResultText(FaceDetail detail) {
        String resultText = "總辨識時間(上傳+辨識+下載) = " + detectResult.executeTime/1000f + " 秒.\n";
        if (null == detail) {
            resultText += "\n偵測不到人臉.\n";
            tv_face_info.setText(resultText);
            return;
        }
        List<FaceDetail> faceDetails = detectResult.detectFacesResult.getFaceDetails();
        resultText += "共偵測到 " + faceDetails.size() + " 張臉.\n\n";

        int originWidth = detectResult.imageSize[0];
        int originHeight = detectResult.imageSize[1];

        resultText += ("準確率 = " + String.format("%.2f", detail.getConfidence()) + "%\n");
        if (null != detail.getBoundingBox()) {
            // Relative coordination need to convert to absolute coordination
            resultText += "位置: {\n";
            resultText += ("\t\tLeft = " + (int) (detail.getBoundingBox().getLeft()*originWidth) + "\n");
            resultText += ("\t\tTop = " + (int) (detail.getBoundingBox().getTop()*originHeight) + "\n");
            resultText += ("\t\tWidth = " + (int) (detail.getBoundingBox().getWidth()*originWidth) + "\n");
            resultText += ("\t\tHeight = " + (int) (detail.getBoundingBox().getHeight()*originHeight) + "\n");
            resultText += "}\n";
        }
        if (null != detail.getAgeRange()) {
            resultText += ("年齡 = " + detail.getAgeRange().getLow() + "~" + detail.getAgeRange().getHigh() + " 歲\n");
        }
        if (null != detail.getGender()) {
            resultText += ("性別 = " + detail.getGender().getValue() + ",\t準確率 = " + String.format("%.2f", detail.getGender().getConfidence()) + "%\n");
        }
        if (null != detail.getEyeglasses()) {
            resultText += ("有戴眼鏡 = " + detail.getEyeglasses().getValue() + ",\t準確率 = " + String.format("%.2f", detail.getEyeglasses().getConfidence()) + "%\n");
        }
        if (null != detail.getSunglasses()) {
            resultText += ("有戴太陽眼鏡 = " + detail.getSunglasses().getValue() + ",\t準確率 = " + String.format("%.2f", detail.getSunglasses().getConfidence()) + "%\n");
        }
        if (null != detail.getMustache()) {
            resultText += ("有（長在鼻子和嘴唇間的）鬍子 = " + detail.getMustache().getValue() + ",\t準確率 = " + String.format("%.2f", detail.getMustache().getConfidence()) + "%\n");
        }
        if (null != detail.getBeard()) {
            resultText += ("有（長在下巴或臉頰的）鬍子 = " + detail.getBeard().getValue() + ",\t準確率 = " + String.format("%.2f", detail.getBeard().getConfidence()) + "%\n");
        }
        if (null != detail.getSmile()) {
            resultText += ("有笑容 = " + detail.getSmile().getValue() + ",\t準確率 = " + String.format("%.2f", detail.getSmile().getConfidence()) + "%\n");
        }
        if (null != detail.getEyesOpen()) {
            resultText += ("眼睛是否張開 = " + detail.getEyesOpen().getValue() + ",\t準確率 = " + String.format("%.2f", detail.getEyesOpen().getConfidence()) + "%\n");
        }
        if (null != detail.getEmotions()) {
            resultText += "心情: {\n";
            List<Emotion> emotions = detail.getEmotions();
            for (int i = 0; i < emotions.size(); i ++) {
                Emotion emotion = emotions.get(i);
                resultText += ("\t\t[" + (i+1) + "] = " + emotion.getType() + ",\t準確率 = " + String.format("%.2f", emotion.getConfidence()) + "%\n");
            }
            resultText += "}\n";
        }
        if (null != detail.getQuality()) {
            resultText += "圖片品質: {\n";
            resultText += ("\t\tBrightness = " + String.format("%.2f", detail.getQuality().getBrightness()) + "%\n");
            resultText += ("\t\tSharpness = " + String.format("%.2f", detail.getQuality().getSharpness()) + "%\n");
            resultText += "}\n";
        }

        tv_face_info.setText(resultText);
    }

    private DetectResult detectResult = new DetectResult();
    private class DetectResult {
        public int[] imageSize;
        public Uri uri;
        public ResourceImage resourceImage;

        public long executeTime;
        public DetectFacesResult detectFacesResult;

        public DetectResult() {
            super();
        }

    }

    private DetectFacesTask detectFacesTask;
    private class DetectFacesTask extends AsyncTask<Void, Void, DetectFacesResult> {

        private Context context;
        private LoadingDialog loadingDialog;
        private DetectFacesRequest request;
        private long timeStart;

        public DetectFacesTask(Context context, Uri uri) {
            super();

            this.context = context;

            if (null != uri) {
                try {
                    byte[] data = Utils.getBytes(context, uri);
                    Image awsImage = new Image().withBytes(ByteBuffer.wrap(data));
                    request = new DetectFacesRequest(awsImage);
                    request.setAttributes(Collections.singletonList("ALL"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        public DetectFacesTask(Context context, ResourceImage resourceImage) {
            super();

            this.context = context;

            if (null != resourceImage) {
                try {
                    byte[] data = resourceImage.getBytes(context);
                    Image awsImage = new Image().withBytes(ByteBuffer.wrap(data));
                    request = new DetectFacesRequest(awsImage);
                    request.setAttributes(Collections.singletonList("ALL"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        @Override
        protected void onPreExecute() {
            Log.e(TAG, "Start");
            timeStart = System.currentTimeMillis();

            loadingDialog = new LoadingDialog(context);
            loadingDialog.setText("辨識中");
            loadingDialog.show();

            MainActivity mainActivity = MainActivity.this;
            mainActivity.ll_small_faces.removeAllViews();
            mainActivity.tv_face_info.setText(null);

            super.onPreExecute();
        }

        @Override
        protected DetectFacesResult doInBackground(Void... params) {

            try {
                AmazonRekognitionClient client = Utils.getRekognitionClient(context);
                return client.detectFaces(request);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(DetectFacesResult detectFacesResult) {
            super.onPostExecute(detectFacesResult);

            Log.e(TAG, "Finished");

            loadingDialog.dismiss();
            loadingDialog = null;

            if (null == detectFacesResult) {
                Log.e(TAG, "No result");

                taskFinished(null);
                return;
            }

            Log.e(TAG, "face result = " + detectFacesResult);

            taskFinished(detectFacesResult);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            Log.e(TAG, "Cancelled");

            loadingDialog.dismiss();
            loadingDialog = null;

            taskFinished(null);
        }

        private void taskFinished(DetectFacesResult detectFacesResult) {
            MainActivity mainActivity = MainActivity.this;

            mainActivity.detectResult.executeTime = System.currentTimeMillis()-timeStart;
            mainActivity.detectResult.detectFacesResult = detectFacesResult;
            mainActivity.configureDetectResult();

            mainActivity.detectFacesTask = null;
        }

    }

}
