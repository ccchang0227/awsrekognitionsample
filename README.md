# AWSRekognitionSample

使用AWS的Rekognition服務來進行人臉的年齡、性別辨識。

## Requirements

Android 4.2 (API 17) 以上

## Author

Chih-chieh Chang, ccch.realtouch@gmail.com

## License

AWSRekognitionSample is available under the MIT license. See the LICENSE file for more info.
