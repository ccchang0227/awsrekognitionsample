package com.realtouchapp.facerekognition;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.realtouchapp.defaultresources.CropImageTransformation;
import com.realtouchapp.defaultresources.LoadingDialog;
import com.realtouchapp.defaultresources.SubHeadView;
import com.realtouchapp.defaultresources.UserInterfaceTool;
import com.realtouchapp.defaultresources.UtilsBase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends Activity {
    private final static String TAG = MainActivity.class.getSimpleName();

    private final static int REQUEST_CAMERA = 5566;
    private final static int SELECT_FILE = 7788;

    private static class ResourceImage {
        public String title;
        public int resID;

        public ResourceImage(String title, int resID) {
            super();

            this.title = title;
            this.resID = resID;
        }

        public int[] getBitmapSize(Context context) {
            int[] size = new int[]{0, 0};
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), resID, options);

                size[0] = options.outWidth;
                size[1] = options.outHeight;

            } catch (Throwable e) {
                e.printStackTrace();
            }

            return size;
        }

        public byte[] getBytes(Context context) {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                Bitmap bm = BitmapFactory.decodeResource(context.getResources(), resID, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                return stream.toByteArray();
            } catch (Throwable e) {
                e.printStackTrace();
            }

            return null;
        }

    }
    private final static ResourceImage resourceImages[] = {
            new ResourceImage("兩個女人", R.mipmap.binfun),
            new ResourceImage("三個人(暗)", R.mipmap.dark_img),
            new ResourceImage("狗狗", R.mipmap.dog),
            new ResourceImage("吉芳", R.mipmap.fang),
            new ResourceImage("可愛的幼女", R.mipmap.loli),
            new ResourceImage("錢錢", R.mipmap.no_face),
            new ResourceImage("柏翰", R.mipmap.pohan),
            new ResourceImage("花形", R.mipmap.shinting),
            new ResourceImage("女人1", R.mipmap.woman1),
            new ResourceImage("女人2", R.mipmap.woman2),
            new ResourceImage("三課", R.mipmap.group),
            new ResourceImage("健中", R.mipmap.group2),
            new ResourceImage("很多人", R.mipmap.many_people)
    };

    private View btn_upload;
    private ImageView iv_origin;
    private TextView tv_origin_image_size;

    private LinearLayout ll_small_faces;
    private TextView tv_face_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iv_origin = (ImageView) findViewById(R.id.iv_origin);
        tv_origin_image_size = (TextView) findViewById(R.id.tv_origin_image_size);

        ll_small_faces = (LinearLayout) findViewById(R.id.ll_small_faces);
        tv_face_info = (TextView) findViewById(R.id.tv_face_info);

        btn_upload = findViewById(R.id.btn_upload);
        registerForContextMenu(btn_upload);
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final CharSequence[] items = { "Take Photo",
                        "Choose from Library",
                        "From Resource",
                        "Cancel" };

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Add Photo!");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @SuppressWarnings("ResultOfMethodCallIgnored")
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        switch (item) {
                            case 0: {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                                try {
                                    if (f.exists()) {
                                        f.delete();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                                startActivityForResult(intent, REQUEST_CAMERA);

                                break;
                            }
                            case 1: {
                                Intent intent = new Intent(
                                        Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                intent.setType("image/*");
                                startActivityForResult(
                                        Intent.createChooser(intent, "Select File"),
                                        SELECT_FILE);

                                break;
                            }
                            case 2: {
                                openContextMenu(v);

                                break;
                            }
                            default: {
                                dialog.dismiss();

                                break;
                            }
                        }

                    }
                });
                builder.show();
            }
        });

    }

    @Override
    protected void onDestroy() {
        unregisterForContextMenu(btn_upload);

        super.onDestroy();
    }

    private MenuItem menu_Cancel;
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        for (int i = 0; i < resourceImages.length; i ++) {
            menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, resourceImages[i].title);
        }
        menu_Cancel = menu.add(Menu.NONE, Menu.FIRST+resourceImages.length, Menu.NONE, "取消");

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (!item.equals(menu_Cancel)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Glide.get(MainActivity.this).clearDiskCache();
                }
            }).start();
            Glide.get(MainActivity.this).clearMemory();

            int index = item.getItemId()-Menu.FIRST;
            ResourceImage resourceImage = resourceImages[index];
            detectResult.uri = null;
            detectResult.resourceImage = resourceImage;

            Glide.with(MainActivity.this)
                    .load(resourceImage.resID)
                    .fitCenter()
                    .into(iv_origin);

            int[] size = resourceImage.getBitmapSize(MainActivity.this);
            tv_origin_image_size.setText("解析度: " + size[0] + "x" + size[1]);
            detectResult.imageSize = size;

            detectFaces(resourceImage);

        }
        menu_Cancel = null;

        return super.onContextItemSelected(item);
    }

    private File getTempPictureFile() {
        File f = new File(Environment.getExternalStorageDirectory().toString());
        for (File temp : f.listFiles()) {
            if (temp.getName().equals("temp.jpg")) {
                return temp;
            }
        }

        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Glide.get(MainActivity.this).clearDiskCache();
                }
            }).start();
            Glide.get(MainActivity.this).clearMemory();

            if (requestCode == REQUEST_CAMERA) {
                File f = getTempPictureFile();
                if (null == f) {
                    return;
                }

                Uri selectedImageUri = Uri.fromFile(f);
                detectResult.uri = selectedImageUri;
                detectResult.resourceImage = null;

                Glide.with(MainActivity.this)
                        .load(selectedImageUri)
                        .fitCenter()
                        .into(iv_origin);

                int[] size = UtilsBase.getBitmapSize(f.getAbsolutePath());
                tv_origin_image_size.setText("解析度: " + size[0] + "x" + size[1]);
                detectResult.imageSize = size;

                detectFaces(selectedImageUri);

            }
            else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                detectResult.uri = selectedImageUri;
                detectResult.resourceImage = null;

                Glide.with(MainActivity.this)
                        .load(selectedImageUri)
                        .fitCenter()
                        .into(iv_origin);

                int[] size = UtilsBase.getBitmapSize(UtilsBase.getPath(MainActivity.this, selectedImageUri));
                tv_origin_image_size.setText("解析度: " + size[0] + "x" + size[1]);
                detectResult.imageSize = size;

                detectFaces(selectedImageUri);

            }

        }

    }

    private void configureDetectResult() {
        if (null == detectResult.detectFacesResult) {
            return;
        }

        JSONArray faceDetails = detectResult.detectFacesResult.optJSONArray("FaceDetails");

        if (null != faceDetails && faceDetails.length() > 0) {

            int originWidth = detectResult.imageSize[0];
            int originHeight = detectResult.imageSize[1];

            int clipImageSize = UserInterfaceTool.getPixelFromDpByDevice(MainActivity.this, 100);

            for (int i = 0; i < faceDetails.length(); i++) {
                JSONObject detail = faceDetails.optJSONObject(i);

                SubHeadView shv_subImg = new SubHeadView(MainActivity.this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(clipImageSize - 20, clipImageSize - 20);
                params.setMargins(10, 10, 10, 10);
                ll_small_faces.addView(shv_subImg, params);

                RectF clipRegion = getClipRegion(detail, originWidth, originHeight);
                BitmapTransformation transformation = new CropImageTransformation(MainActivity.this, clipRegion, i, detectResult.imageSize);

                if (null != detectResult.uri) {
                    Glide.with(MainActivity.this)
                            .load(detectResult.uri)
                            .transform(transformation)
                            .into(shv_subImg.iv_content);
                } else if (null != detectResult.resourceImage) {
                    Glide.with(MainActivity.this)
                            .load(detectResult.resourceImage.resID)
                            .transform(transformation)
                            .into(shv_subImg.iv_content);
                }

                shv_subImg.setTag(i + 1);
                shv_subImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int i = 0; i < ll_small_faces.getChildCount(); i++) {
                            View child = ll_small_faces.getChildAt(i);
                            if (child == v) {
                                child.setBackgroundColor(Color.RED);
                            } else {
                                child.setBackgroundColor(Color.TRANSPARENT);
                            }
                        }

                        int index = (int) v.getTag() - 1;
                        JSONArray faceDetails = detectResult.detectFacesResult.optJSONArray("FaceDetails");
                        JSONObject detail = faceDetails.optJSONObject(index);
                        configureResultText(detail);
                    }
                });

                if (i == 0) {
                    shv_subImg.setBackgroundColor(Color.RED);
                    configureResultText(detail);
                } else {
                    shv_subImg.setBackgroundColor(Color.TRANSPARENT);
                }

            }

        } else {
            configureResultText(null);
        }
    }

    private RectF getClipRegion(JSONObject detail, int originWidth, int originHeight) {
        if (null == detail) {
            return new RectF(0, 0, originWidth, originHeight);
        }
        JSONObject boundingBox = detail.optJSONObject("BoundingBox");
        if (null == boundingBox) {
            return new RectF(0, 0, originWidth, originHeight);
        }

        float left = (float) (boundingBox.optDouble("x", 0)*originWidth);
        if (left < 0) {
            left = 0;
        }
        float top = (float) (boundingBox.optDouble("y", 0)*originHeight);
        if (top < 0) {
            top = 0;
        }
        float right = (float) (left + boundingBox.optDouble("w", 0)*originWidth);
        if (right > originWidth) {
            right = originWidth;
        }
        float bottom = (float) (top + boundingBox.optDouble("h", 0)*originHeight);
        if (bottom > originHeight) {
            bottom = originHeight;
        }

        return new RectF(left, top, right, bottom);
    }

    @SuppressLint("DefaultLocale")
    private void configureResultText(JSONObject detail) {
        String resultText = "總辨識時間(上傳+辨識+下載) = " + detectResult.executeTime/1000f + " 秒.\n";
        if (null == detail) {
            resultText += "\n偵測不到人臉.\n";
            tv_face_info.setText(resultText);
            return;
        }
        JSONArray faceDetails = detectResult.detectFacesResult.optJSONArray("FaceDetails");
        resultText += "共偵測到 " + faceDetails.length() + " 張臉.\n\n";

        int originWidth = detectResult.imageSize[0];
        int originHeight = detectResult.imageSize[1];

        resultText += ("準確率 = " + String.format("%.2f", detail.optDouble("Confidence", 0)) + "%\n");
        if (null != detail.opt("BoundingBox")) {
            JSONObject boundingBox = detail.optJSONObject("BoundingBox");
            // Relative coordination need to convert to absolute coordination
            resultText += "位置: {\n";
            resultText += ("\t\tLeft = " + (int) (boundingBox.optDouble("x", 0)*originWidth) + "\n");
            resultText += ("\t\tTop = " + (int) (boundingBox.optDouble("y", 0)*originHeight) + "\n");
            resultText += ("\t\tWidth = " + (int) (boundingBox.optDouble("w", 0)*originWidth) + "\n");
            resultText += ("\t\tHeight = " + (int) (boundingBox.optDouble("h", 0)*originHeight) + "\n");
            resultText += "}\n";
        }
        if (null != detail.opt("AgeRange")) {
            JSONObject ageRange = detail.optJSONObject("AgeRange");
            resultText += ("年齡 = " + ageRange.optInt("Low", 0) + "~" + ageRange.optInt("High", 0) + " 歲\n");
        }
        if (null != detail.opt("Gender")) {
            resultText += ("性別 = " + detail.optString("Gender", "???") + "\n");
        }
        if (null != detail.opt("Quality")) {
            JSONObject quality = detail.optJSONObject("Quality");
            resultText += "圖片品質: {\n";
            resultText += ("\t\tBrightness = " + String.format("%.2f", quality.optDouble("Brightness", 0)) + "%\n");
            resultText += ("\t\tSharpness = " + String.format("%.2f", quality.optDouble("Sharpness", 0)) + "%\n");
            resultText += "}\n";
        }

        tv_face_info.setText(resultText);
    }

    private DetectResult detectResult = new DetectResult();
    private class DetectResult {
        public int[] imageSize;
        public Uri uri;
        public ResourceImage resourceImage;

        public long executeTime;
        public JSONObject detectFacesResult;

        public DetectResult() {
            super();
        }

    }

    private LoadingDialog loadingDialog;
    private long timeStart;

    private void detectFaces(Uri uri) {
        if (null == uri) {
            return;
        }

        try {
            byte[] data = UtilsBase.getBytes(MainActivity.this, uri);
            detectFacesViaRESTfulAPI(data);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void detectFaces(ResourceImage resourceImage) {
        if (null == resourceImage) {
            return;
        }

        byte[] data = resourceImage.getBytes(MainActivity.this);
        detectFacesViaRESTfulAPI(data);

    }

    private void detectFacesViaRESTfulAPI(byte[] content) {
        timeStart = System.currentTimeMillis();

        loadingDialog = new LoadingDialog(MainActivity.this);
        loadingDialog.setText("辨識中");
        loadingDialog.show();

        ll_small_faces.removeAllViews();
        tv_face_info.setText(null);

        try {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(false)
                    .build();

            MultipartBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addPart(Headers.of("Content-Disposition", "form-data; name=\"file\"; filename=\"test\""),
                            RequestBody.create(MediaType.parse("image/jpg"), content))
                    .build();

            Request request = new Request.Builder()
                    .url("http://ec2-52-91-159-168.compute-1.amazonaws.com/rekognition/sample.php")
                    .post(body)
                    .build();

//            MultipartBody body = new MultipartBody.Builder()
//                    .setType(MultipartBody.FORM)
//                    .addPart(Headers.of("Content-Disposition", "form-data; name=\"file\"; filename=\"test\""),
//                            RequestBody.create(MediaType.parse("image/jpg"), content))
//                    .addFormDataPart("organizerNO", "20170602030636K2D88Q14")
//                    .build();
//
//            Request request = new Request.Builder()
//                    .url("http://dev.realtouchapp.com/api/business/v1/android/en/manage/sales/facerecognition")
//                    .post(body)
//                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();

                    loadingDialog.dismiss();
                    loadingDialog = null;

                    MainActivity.this.requestFinished(null);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.e("AWS", "response = "+response.body().string());
                    loadingDialog.dismiss();
                    loadingDialog = null;

                    try {
                        MainActivity.this.requestFinished(response.body().string());
                    } catch (Exception e) {
                        e.printStackTrace();

                        MainActivity.this.requestFinished(null);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

            loadingDialog.dismiss();
            loadingDialog = null;
        }

    }

    private void requestFinished(String responseString) {
        detectResult.executeTime = System.currentTimeMillis()-timeStart;
        if (null == responseString || responseString.length() == 0) {
            detectResult.detectFacesResult = null;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    configureDetectResult();
                }
            });

            return;
        }

        Log.e(TAG, "response = " + responseString);

        try {
            detectResult.detectFacesResult = new JSONObject(responseString);
        } catch (JSONException e) {
            e.printStackTrace();

            detectResult.detectFacesResult = null;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                configureDetectResult();
            }
        });

    }

}
